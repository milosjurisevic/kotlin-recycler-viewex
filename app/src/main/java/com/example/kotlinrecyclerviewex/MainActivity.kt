package com.example.kotlinrecyclerviewex

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {

    private lateinit var blogAdapter: BlogRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
        addDataSet()

    }

    private fun addDataSet() {
    val data = DataSource.createDataSet()
        blogAdapter.submitList(data)

    }

    private fun initRecyclerView() {
        var recycler_view: RecyclerView? = null
        recycler_view = findViewById<RecyclerView>(R.id.recycler_view)

//        Jedan nacin:
        recycler_view.layoutManager = LinearLayoutManager(this@MainActivity)
        val topSpacingItemDecoration = TopSpacingItemDecoration(30)
//        ad = (topSpacingItemDecoration)
        blogAdapter = BlogRecyclerAdapter()
        recycler_view.adapter = blogAdapter


//        Drugi nacin:
//        recycler_view.apply {
//            RecyclerView.LayoutManager = LinearLayoutManager(this@MainActivity)
//            blogAdapter = BlogRecyclerAdapter()
//            adapter = blogAdapter
        }
    }
